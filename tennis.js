let numerOfTimesPlayer1Scored
let numerOfTimesPlayer2Scored

export function startGame() { 
    numerOfTimesPlayer1Scored = 0 
    numerOfTimesPlayer2Scored = 0 
}

export function incrementPlayer1Score() {
    numerOfTimesPlayer1Scored += 1
}

export function incrementPlayer2Score() {
    numerOfTimesPlayer2Scored += 1
}

export function getScore() {
    if(typeof(numerOfTimesPlayer1Scored) == "undefined" || typeof(numerOfTimesPlayer2Scored) == "undefined") {
        throw new Error("Game is not started")
    }
    if(numerOfTimesPlayer1Scored > 3|| numerOfTimesPlayer2Scored > 3) {
        return translateScoreAfterThreeTimesScoredByAPlayer()
    } 
    return translateScoreBeforeThreeTimesScoredByAPlayer(numerOfTimesPlayer1Scored) + " - " + translateScoreBeforeThreeTimesScoredByAPlayer(numerOfTimesPlayer2Scored)
}

/**
 * Returns a string with the player that wins i.e. "player 1" or "player 2"
 */
export function checkPlayerWins() {
    if(numerOfTimesPlayer1Scored > 3 && numerOfTimesPlayer1Scored - 2 >= numerOfTimesPlayer2Scored) {
        return "player 1"
    } else if (numerOfTimesPlayer2Scored > 3 && numerOfTimesPlayer2Scored - 2 >= numerOfTimesPlayer1Scored) {
        return "player 2"
    }
}

function translateScoreBeforeThreeTimesScoredByAPlayer(numberOfTimesPlayerScored) {
    switch(numberOfTimesPlayerScored) {
        case 1:
            return 15
        case 2:
            return 30
        case 3: 
            return 40
        case 0:
        default:
            return 0
    }
}

function translateScoreAfterThreeTimesScoredByAPlayer() {
    if (numerOfTimesPlayer1Scored === numerOfTimesPlayer2Scored) {
        return "40 - 40"
    } else if (numerOfTimesPlayer1Scored > numerOfTimesPlayer2Scored) {
        return "A - 40"
    } else if (numerOfTimesPlayer2Scored > numerOfTimesPlayer1Scored) {
        return "40 - A"
    }
}
