import { startGame, incrementPlayer1Score, incrementPlayer2Score, getScore, checkPlayerWins } from './tennis';

describe("Tennis Game tests", () => {
    test('When game is not started and score is get', () => {
        expect(getScore).toThrowError(new Error("Game is not started"))
    });

    test('When game starts, score is 0 - 0', () => {
        startGame()
        expect(getScore()).toBe("0 - 0")
    });

    test('When player1 scores, score is 15 - 0', () => {
        startGame()
        incrementPlayer1Score()
        expect(getScore()).toBe("15 - 0")
    });

    test('When player1 scores twice, score is 30 - 0', () => {
        startGame()
        incrementPlayer1Score()
        incrementPlayer1Score()
        expect(getScore()).toBe("30 - 0")
    });

    test('When player1 scores three times, score is 40 - 0', () => {
        startGame()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        expect(getScore()).toBe("40 - 0")
    });

    test('When player2 scores, score is 0 - 15', () => {
        startGame()
        incrementPlayer2Score()
        expect(getScore()).toBe("0 - 15")
    });

    test('When player2 scores twice, score is 0 - 30', () => {
        startGame()
        incrementPlayer2Score()
        incrementPlayer2Score()
        expect(getScore()).toBe("0 - 30")
    });

    test('When player2 scores three times, score is 0 - 40', () => {
        startGame()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        expect(getScore()).toBe("0 - 40")
    });

    test('When player1 scores and player 2 scores, score is 15 - 15', () => {
        startGame()
        incrementPlayer1Score()
        incrementPlayer2Score()
        expect(getScore()).toBe("15 - 15")
    });

    test('When player1 scores twice and player 2 scores twice, score is 30 - 30', () => {
        startGame()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        expect(getScore()).toBe("30 - 30")
    });

    test('When player1 scores threes time and player 2 scores three times, score is 40 - 40', () => {
        startGame()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        expect(getScore()).toBe("40 - 40")
    });

    test('When player1 scores three times and player 2 scores four times, score is 40 - A', () => {
        startGame()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        expect(getScore()).toBe("40 - A")
    });

    test('When player2 scores three times and player1 scores four times, score is A - 40', () => {
        startGame()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        expect(getScore()).toBe("A - 40")
    });

    test('When player2 scores three times and player1 scores four times, and player2 scores, score is 40 - 40', () => {
        startGame()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer2Score()
        expect(getScore()).toBe("40 - 40")
    });

    test('When player1 scores four times, he wins the game', () => {
        startGame()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        expect(checkPlayerWins()).toBe("player 1")
    });

    test('When player2 scores four times, he wins the game', () => {
        startGame()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        expect(checkPlayerWins()).toBe("player 2")
    });

    test('When player2 scores three times and player 1 scores five times, player 1 wins the game', () => {
        startGame()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        expect(checkPlayerWins()).toBe("player 1")
    });

    test('When player1 scores three times and player 2 scores five times, player 2 wins the game', () => {
        startGame()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer1Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        incrementPlayer2Score()
        expect(checkPlayerWins()).toBe("player 2")
    });
})

